<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{HomeController,SliderController,ProfileController,GaleriController,ProdukController,PostController};
use App\Http\Controllers\Auth\RegisteredUserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('pages.dashboard');
// });
Route::get('/',[HomeController::class,'home'])->name('home');
Route::get('/tentangkami',[HomeController::class,'profile'])->name('profil');
Route::get('/galeri',[HomeController::class,'galeri'])->name('galeri');
Route::get('/kontak',[HomeController::class,'kontak'])->name('kontak');
Route::get('/produks/{produk:slug}',[HomeController::class,'detail'])->name('produk');
Route::get('/informasi/{post:slug}',[HomeController::class,'informasiDetail'])->name('informasi.detail');
Route::get('/informasi',[HomeController::class,'informasi'])->name('informasi');

Route::group(['middleware' => 'auth'],function(){
    Route::get('/dashboard', [HomeController::class,'dashboard'])->name('dashboard');

    Route::group(['prefix' => 'profile'],function(){
        Route::get('/mbsprofile',[ProfileController::class,'index'])->name('profile');
        Route::get('/{profile:id}/edit',[ProfileController::class,'edit'])->name('edit.profile');
        Route::patch('/{profile:id}/edit',[ProfileController::class,'update']);
    });

    Route::group(['prefix' => 'slider'],function(){
        Route::get('/lists',[SliderController::class,'index'])->name('sliders');
        Route::get('/create',[SliderController::class,'create'])->name('create.slider');
        Route::post('/create',[SliderController::class,'store']);
        Route::get('/{slider:id}/edit',[SliderController::class,'edit'])->name('edit.slider');
        Route::patch('/{slider:id}/edit',[SliderController::class,'update']);
        Route::delete('/delete',[SliderController::class,'destroy'])->name('delete.slider');
    });

    Route::group(['prefix' => 'produk'],function(){
        Route::get('/lists',[ProdukController::class,'index'])->name('produks');
        Route::get('/create',[ProdukController::class,'create'])->name('create.produk');
        Route::post('/create',[ProdukController::class,'store']);
        Route::get('/{produk:id}/edit',[ProdukController::class,'edit'])->name('edit.produk');
        Route::patch('/{produk:id}/edit',[ProdukController::class,'update']);
        Route::delete('/delete',[ProdukController::class,'destroy'])->name('delete.produk');
        Route::post('/images', [ProdukController::class,'upload'])->name('post.image');
    });

    Route::group(['prefix' => 'galeri'],function(){
        Route::get('/lists',[GaleriController::class,'index'])->name('galeris');
        Route::get('/create',[GaleriController::class,'create'])->name('create.galeri');
        Route::post('/create',[GaleriController::class,'store']);
        // Route::get('/{galeri:id}/edit',[GaleriController::class,'edit'])->name('edit.galeri');
        // Route::patch('/{galeri:id}/edit',[GaleriController::class,'update']);
        Route::delete('/delete',[GaleriController::class,'destroy'])->name('delete.galeri');
    });
     //user
    Route::group(['prefix' => 'users'],function(){
        Route::get('/register', [RegisteredUserController::class, 'create'])->name('register');
        Route::post('/register', [RegisteredUserController::class, 'store'])->name('create.register');
        Route::get('/list-user', [RegisteredUserController::class, 'index'])->name('list-user');
        Route::delete('/delete', [RegisteredUserController::class, 'destroy'])->name('delete.user');
    });

    //post
    Route::group(['prefix' => 'post'],function(){
        Route::get('/lists',[PostController::class,'index'])->name('artikel');
        Route::get('/create',[PostController::class,'create'])->name('create.artikel');
        Route::post('/create',[PostController::class,'store']);
        Route::get('/{post:id}/edit',[PostController::class,'edit'])->name('edit.artikel');
        Route::patch('/{post:id}/edit',[PostController::class,'update']);
        Route::delete('/delete',[PostController::class,'destroy'])->name('delete.artikel');
    });


});

require __DIR__.'/auth.php';
