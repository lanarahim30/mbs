<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>ADMIN | MBS</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="{{asset('css/bootstrap/bootstrap.min.css')}}">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap/jquery.mCustomScrollbar.min.css')}}">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

</head>

<body>
    
    <div class="wrapper">
        <!-- Sidebar  -->
        @include('components.sidebar')
        <!-- Page Content  -->
        <div id="content">
            @include('components.alert')
            @yield('content')
        </div>
    </div>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="{{asset('js/slim.min.js')}}" ></script>
    <!-- Popper.JS -->
    <script src="{{asset('js/popper.min.js')}}"></script>
    <!-- Bootstrap JS -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
        $(document).on('click','.deleteSlider',function(){
            var sliderID=$(this).attr('data-sliderid');
            // console.log(sliderID);
            $('#slider_id').val(sliderID); 
            $('#deleteSlider').modal('show'); 
        });
        $(document).on('click','.deleteUser',function(){
            var artikel=$(this).attr('data-user');
            // console.log(sliderID);
            $('#user_id').val(artikel); 
            $('#deleteUser').modal('show'); 
        });
        $(document).on('click','.deleteGaleri',function(){
            var galeri=$(this).attr('data-galeriId');
            // console.log(sliderID);
            $('#galeri_id').val(galeri); 
            $('#deleteGaleri').modal('show'); 
        });
        $(document).on('click','.deleteProduct',function(){
            var productID=$(this).attr('data-product');
            // console.log(sliderID);
            $('#product_id').val(productID); 
            $('#deleteProduct').modal('show'); 
        });
        $(document).on('click','.deleteArtikel',function(){
            var artikel=$(this).attr('data-artikel');
            // console.log(sliderID);
            $('#artikel_id').val(artikel); 
            $('#deleteArtikel').modal('show'); 
        });
        $(document).ready(function() {
            $(".btn-success").click(function(){ 
                var html = $(".clone").html();
                $(".increment").after(html);
            });
            $("body").on("click",".btn-danger",function(){ 
                $(this).parents(".control-group").remove();
            });
            
        });
    </script>
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
    
    <script>
    var konten = document.getElementById("profile");
        CKEDITOR.replace(konten,{
        filebrowserUploadUrl: "{{route('post.image', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
    CKEDITOR.config.allowedContent = true;
    </script>
</body>

</html>