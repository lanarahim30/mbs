<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel = "icon" href ="{{asset('storage/logo/logo.jpeg')}}"  type = "image/x-icon"> 
    <title>MBS</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/style2.css?v=1.1')}}" type="text/css">
</head>
<body>
    @include('components.navbar')
    
    @yield('content')
    <div class="bg-dark py-3" id="toko">
        <div class="container">
            <div class="text-center text-white online-shop">
                <h3>Alamat Kantor Pusat</h3>
                <p>Desa Candikuning, Kecamatan Baturiti, Kabupaten Tabanan, Bali</p>
            </div>
        </div>
    </div>
<footer class="py-2 bg-custom">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Mercusuar Buana Sejahtera</p>
    </div>
    <!-- /.container -->
</footer>
</body>
<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
</html>
