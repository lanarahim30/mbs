<nav class="navbar navbar-expand-lg navbar-light bg-light navbar-fixed-top">
  <div class="container">
    <a class="navbar-brand" href="{{route('home')}}">
      <img src="{{asset('storage/logo/logo.jpeg')}}" alt="" width="50" height="50">
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link {{ request()->is('/') ? 'active' : ''}}" aria-current="page" href="{{route('home')}}">Beranda</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ request()->is('tentangkami') ? 'active' : ''}}" href="{{route('profil')}}">Tentang Kami</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ request()->is('galeri') ? 'active' : ''}}" href="{{route('galeri')}}">Galeri</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle {{ request()->is('produks*') ? 'active' : ''}}" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Produk
          </a>
          
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            @foreach($produks as $key=>$produk)
              <li><a class="dropdown-item" href="{{route('produk',$produk)}}">{{$produk->name}}</a></li>
            @endforeach
            </ul>
          
        </li>
        <li class="nav-item">
          <a class="nav-link {{ request()->is('kontak') ? 'active' : ''}}" href="{{route('kontak')}}">Kontak</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ request()->is('informasi') ? 'active' : ''}}" href="{{route('informasi')}}">Informasi</a>
        </li>
        <li class="nav-item">
          <a class="btn btn-primary btn-custom" href="{{route('login')}}">Login</a>
        </li>
      </ul>
    </div>
  </div>
</nav>