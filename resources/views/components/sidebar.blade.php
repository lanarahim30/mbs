<nav id="sidebar">
    <div class="sidebar-header">
        <a href="{{route('dashboard')}}">
            <h3>MBS</h3>
        </a>
    </div>

    <ul class="list-unstyled components">
        <p>Mercusuar Buana Sejahtera</p>
        <li class="{{ request()->is('slider/*') ? 'active' : '' }}">
            <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Slider</a>
            <ul class="collapse list-unstyled" id="homeSubmenu">
                <li>
                    <a href="{{ route('create.slider')}} ">Tambah Slider</a>
                </li>
                <li>
                    <a href="{{ route('sliders')}} ">Lihat Slider</a>
                </li>
            </ul>
        </li>
        <li class="{{ request()->is('galeri/*') ? 'active' : '' }}">
            <a href="#galeriSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Galeri</a>
            <ul class="collapse list-unstyled" id="galeriSubmenu">
                <li>
                    <a href="{{ route('create.galeri')}} ">Tambah Galeri</a>
                </li>
                <li>
                    <a href="{{ route('galeris')}} ">Lihat Galeri</a>
                </li>
            </ul>
        </li>
        <li class="{{ request()->is('produk/*') ? 'active' : '' }}">
            <a href="#produkSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Produk</a>
            <ul class="collapse list-unstyled" id="produkSubmenu">
                <li>
                    <a href="{{ route('create.produk')}} ">Tambah Produk</a>
                </li>
                <li>
                    <a href="{{ route('produks')}} ">Lihat Produk</a>
                </li>
            </ul>
        </li>
        <li class="{{ request()->is('profile/*') ? 'active' : '' }}">
            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Profile</a>
            <ul class="collapse list-unstyled" id="pageSubmenu">
                <li>
                    <a href="{{ route('profile')}}">Lihat Profile</a>
                </li>
            </ul>
        </li>
        <li class="{{ request()->is('post/*') ? 'active' : '' }}">
            <a href="#artikelSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Artikel</a>
            <ul class="collapse list-unstyled" id="artikelSubmenu">
                <li>
                    <a href="{{route('create.artikel')}}">Tambah Artikel</a>
                </li>
                <li>
                    <a href="{{ route('artikel')}}">List Artikel</a>
                </li>
            </ul>
        </li>
        <li class="{{ request()->is('users/*') ? 'active' : '' }}">
            <a href="#usersSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Users</a>
            <ul class="collapse list-unstyled" id="usersSubmenu">
                <li>
                    <a href="{{ route('register')}}">Tambah User</a>
                </li>
                <li>
                    <a href="{{ route('list-user')}}">Lihat User</a>
                </li>
            </ul>
        </li>
        <li>
            <form method="POST" action="{{ route('logout') }}">
                @csrf

                <button type="submit" class="form-control underline text-sm text-gray-600 hover:text-gray-900">
                    {{ __('Logout') }}
                </button>
            </form>
        </li>
    </ul>

</nav>