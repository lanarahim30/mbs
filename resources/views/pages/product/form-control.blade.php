<div class="form-group">
    <label for="image">Gambar Product</label>
    <input type="file" class="form-control-file" id="image" name="image">
    @error('image')
    <div class="text-danger mt-2">
        {{$message}}
    </div>
    @enderror
    @if(isset($product->image))
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="{{asset('storage/'.$product->image)}}" alt="Card image cap">
        </div>
    @endif
</div>
<div class="form-group">
    <label for="name">Nama Produk</label>
    <input type="text" class="form-control" name="name" placeholder="Masukan nama produk" value="{{ old('name') ?? $product->name}}">
    @error('name')
    <div class="text-danger mt-2">
        {{$message}}
    </div>
    @enderror
</div>
<div class="form-group">
    <label for="description">Deskripsi</label>
    <textarea name="description" id="profile" cols="30" rows="10" class="form-control">{{old('description') ?? $product->description}}</textarea>
    @error('description')
        <div class="text-danger mt-2">
            {{$message}}
        </div>
    @enderror
</div>
<hr>
<div class="text-center mt-4">
    <a href="{{URL::previous()}}" class="btn btn-warning">Kembali</a>
    <button type="submit" class="btn btn-primary">{{isset($submit) ? $submit : 'Save'}}</button>
</div>