@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="text-center title">
                    <h1>Tambah Product</h1>
                </div>
                <form action="{{route('edit.produk',$product)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    @include('pages.product.form-control')
                </form>
            </div>
        </div>
    </div>
@endsection