@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="text-center title">
                    <h1>Tambah Product</h1>
                </div>
                <form action="{{route('create.produk')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @include('pages.product.form-control')
                </form>
            </div>
        </div>
    </div>
@endsection