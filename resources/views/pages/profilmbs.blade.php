@extends('layouts.home')
@section('content')
<div class="bg-white">
    <div class="my-3 mx-3">
        <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='currentColor'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tentang Kami</li>
        </ol>
        </nav>
    </div>
</div>
<div class="jumbotron bg-light" style="color:black">
    <div class="text-center">
        <h2>PROFIL</h2>
        <img src="{{asset('storage/logo/logo.jpeg')}}" alt="logo" class="img-fluid" width="400" height="300">
        <p>{!! $image->profile !!}</p>
    </div>
</div>
<div id="portfolio" class="container-fluid text-center bg-grey mt-5">
    <h2>Peta Lokasi</h2>
    <br>
    <div class="mapouter">
        <div class="gmap_canvas">
        <iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=kantor%20konsultan%20mbs&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
        </iframe><a href="https://sites.google.com/view/fmovies-to-org/fmovies"></a>
        </div>
        <!-- <style>.mapouter{position:relative;text-align:right;height:500px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:100%;}</style> -->
    </div>
</div>
@endsection