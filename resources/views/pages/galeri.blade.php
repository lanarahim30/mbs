@extends('layouts.home')
@section('content')
<div class="bg-white">
    <div class="my-3 mx-3">
        <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='currentColor'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Galeri</li>
        </ol>
        </nav>
    </div>
</div>
<div class="mt-5"> 
    <div class="text-center" style="background-color:black;color:white">
        <h2 class="py-3">Galeri Kegiatan</h2>
    </div>
</div>
<div class="jumbotron bg-grey" style="color:black">
    <div class="row text-center slideanim">
        @foreach($images as $key=>$image)
            <div class="col-sm-2">
            <div class="thumbnail">
                <a target="_blank" href="{{asset('storage/'.$image->image)}}">
                    <img src="{{asset('storage/'.$image->image)}}" alt="Paris" class="img-fluid" width="400">
                </a>
                <p><strong>{{$image->name}}</strong></p>
            </div>
            </div>
        @endforeach
    </div>
    <div class="d-flex justify-content-center">
        <div>
            {{ $images->links() }}
        </div>
    </div>
</div>
@endsection