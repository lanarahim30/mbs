@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="text-center title">
                    <h1>Tambah Artikel</h1>
                </div>
                <form action="{{route('create.artikel')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @include('pages.artikel.form-control')
                </form>
            </div>
        </div>
    </div>
@endsection