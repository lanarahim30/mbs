<div class="form-group">
        <label for="image">Gambar Artikel</label>
        <input type="file" class="form-control-file" id="image" name="image">
        @error('image')
        <div class="text-danger mt-2">
            {{$message}}
        </div>
        @enderror
        @if(isset($artikel->image))
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="{{asset('storage/'.$artikel->image)}}" alt="Card image cap">
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="name">Judul Artikel</label>
        <input type="text" class="form-control" name="title" placeholder="Masukan judul" value="{{ old('title') ?? $artikel->title}}">
        @error('title')
        <div class="text-danger mt-2">
            {{$message}}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="description">Deskripsi</label>
        <textarea name="body" id="profile" cols="30" rows="10" class="form-control">{{old('body') ?? $artikel->body}}</textarea>
        @error('body')
            <div class="text-danger mt-2">
                {{$message}}
            </div>
        @enderror
    </div>
<hr>
<div class="text-center mt-4">
    <a href="{{URL::previous()}}" class="btn btn-warning">Kembali</a>
    <button type="submit" class="btn btn-primary">{{isset($submit) ? $submit : 'Save'}}</button>
</div>