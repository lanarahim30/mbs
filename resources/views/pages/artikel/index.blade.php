@extends('layouts.admin')
@section('content')
<div class="container">
    @if($artikels->count())
        <div class="row">
            <div class="col-md-12">
                <div class="text-center title mb-5">
                    <h1>List Artikel</h1>
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Deskripsi</th>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($artikels as $key=>$artikel)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$artikel->title}}</td>
                            <td>{!! \Illuminate\Support\Str::words($artikel->body,10,'.....') !!}</td>
                            <td>{{$artikel->created_at->format('d, M Y')}}</td>
                            <td>
                                <div class="d-flex">
                                    <a href="{{route('edit.artikel',$artikel)}}" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                                    <a href="" class="btn btn-sm btn-info"><i class="fas fa-eye"></i></a>
                                    <button class="btn btn-sm btn-danger deleteArtikel" data-artikel="{{$artikel->id}}"><i class="fas fa-trash"></i></button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @else
            <div class="alert alert-info">
                                
                Artikel kosong

            </div>
    @endif
</div>
<div class="d-flex justify-content-center">
    <div>
        {{ $artikels->links() }}
    </div>
</div>
  <!-- modal  -->
  <div class="modal" tabindex="-1" id="deleteArtikel">
        <div class="modal-dialog">
            <div class="modal-content">
            <form action="{{route('delete.artikel')}}" method="POST">
                @method('delete')
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Hapus Produk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                        <input type="hidden" name="id" id="artikel_id">
                        <p>Anda ingin menghapus artikel ini !!!.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection