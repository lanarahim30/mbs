@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="text-center title">
                    <h1>Edit Kategori</h1>
                </div>
                <form action="{{route('edit.artikel',$artikel)}}" method="POST" enctype="multipart/form-data">
                    @method('patch')
                    @csrf
                    @include('pages.artikel.form-control',['submit' => 'Edit'])
                </form>
            </div>
        </div>
    </div>
@endsection