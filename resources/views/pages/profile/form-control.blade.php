<div class="form-group">
        <label for="profile">Profile</label>
        <textarea name="profile" id="profile" cols="30" rows="10" class="form-control">{{old('profile') ?? $profile->$profile}}</textarea>
        @error('profile')
            <div class="text-danger mt-2">
                {{$message}}
            </div>
        @enderror
    </div>
<hr>
<div class="text-center mt-4">
    <a href="{{URL::previous()}}" class="btn btn-warning">Kembali</a>
    <button type="submit" class="btn btn-primary">{{isset($submit) ? $submit : 'Simpan'}}</button>
</div>