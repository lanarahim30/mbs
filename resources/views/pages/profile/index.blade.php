@extends('layouts.admin')
@section('content')
    @foreach($profiles as $profile)
    <form action="{{route('edit.profile',$profile)}}" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        <div class="form-group">
            <label for="profile">Profil</label>
            <textarea name="profile" id="profile" cols="30" rows="10" class="form-control">{{$profile->profile}}</textarea>
            @error('profile')
                <div class="text-danger mt-2">
                    {{$message}}
                </div>
            @enderror
        </div>
        <div class="text-center mt-4">
            <button type="submit" class="btn btn-primary">{{isset($submit) ? $submit : 'Simpan'}}</button>
        </div>
    </form>
    @endforeach
@endsection