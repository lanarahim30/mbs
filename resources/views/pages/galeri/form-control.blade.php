<div class="form-group ">
    <label for="image">Galeri</label>
    <div class="input-group control-group increment" >
        <input type="file" name="images[]" class="form-control">
        <div class="input-group-btn"> 
        <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
        </div>
    </div>
    <div class="clone hide">
        <div class="control-group input-group" style="margin-top:10px">
        <input type="file" name="images[]" class="form-control">
        <div class="input-group-btn"> 
            <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
        </div>
        </div>
    </div>
    @error('image')
    <div class="text-danger mt-2">
        {{$message}}
    </div>
    @enderror
</div>
<hr>
<div class="text-center mt-4">
    <a href="{{URL::previous()}}" class="btn btn-warning">Kembali</a>
    <button type="submit" class="btn btn-primary">{{isset($submit) ? $submit : 'Save'}}</button>
</div>
