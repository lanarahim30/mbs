@extends('layouts.admin')
@section('content')
    <div class="container">
    @if($galeris->count())
        <div class="text-center title mb-5">
            <h1>List Foto</h1>
        </div>
        <div class="row">
            @foreach ($galeris as $galeri)
                <div class="col-md-2 mb-2">
                    <div class="card">
                        <img class="card-img-top" src="{{asset('storage/'.$galeri->image)}}" alt="{{$galeri->image}}">
                        <div class="card-footer d-flex justify-content-between">
                            <button class="btn btn-sm btn-danger deleteGaleri" data-galeriId="{{$galeri->id}}"><i class="fas fa-trash"></i></button>
                        </div>
                    </div>
                </div>
            @endforeach
            @else 
                <div class="alert alert-info">
                    
                    Foto kosong

                </div>
            @endif
        </div>
        <div class="d-flex justify-content-center">
            <div>
                {{ $galeris->links() }}
            </div>
        </div>
    </div>
    <!-- modal  -->
    <div class="modal" tabindex="-1" id="deleteGaleri">
        <div class="modal-dialog">
            <div class="modal-content">
            <form action="{{route('delete.galeri')}}" method="POST">
                @method('delete')
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Delete Galeri</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                        <input type="hidden" name="id" id="galeri_id">
                        <p>Anda ingin menghapus galeri ini !!!.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection