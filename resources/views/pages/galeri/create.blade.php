@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="text-center title">
                    <h1>Tambah Galeri</h1>
                </div>
                <form action="{{route('create.galeri')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @include('pages.galeri.form-control')
                </form>
            </div>
        </div>
    </div>
@endsection