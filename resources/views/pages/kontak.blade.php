@extends('layouts.home')
@section('content')
<div class="bg-white">
    <div class="my-3 mx-3">
        <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='currentColor'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Kontak</li>
        </ol>
        </nav>
    </div>
</div>
<div id="portfolio" class="container-fluid text-center bg-grey mt-5">
    <div class="mapouter">
        <div class="gmap_canvas">
        <iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=kantor%20konsultan%20mbs&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
        </iframe><a href="https://sites.google.com/view/fmovies-to-org/fmovies"></a>
        </div>
        <!-- <style>.mapouter{position:relative;text-align:right;height:500px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:100%;}</style> -->
    </div>
</div>
<div class="jumbotron bg-kontak" style="color:white">
    <div class="text-center">
        <h2>KONTAK PERSON</h2>
        <div class="row mt-5">
            <div class="col-md-2">
                <img src="{{asset('storage/logo/kontak.png')}}" alt="kontak" class="img-fluid img-kontak" srcset="">
                <div class="mt-3">
                    <h5>082 147 078 722</h5>
                </div>
            </div>
            <div class="col-md-2">
                <img src="{{asset('storage/logo/email.jpg')}}" alt="kontak" class="img-fluid img-kontak" srcset="">
                <div class="mt-3">
                    <h5>info@menara.store</h5>
                </div>
            </div>
            <div class="col-md-3">
                <img src="{{asset('storage/logo/facebook.png')}}" alt="kontak" class="img-fluid img-custom" srcset="">
                <div class="mt-3">
                    <h5>Mbs(Mercusuar Buana Sejahtera)</h5>
                </div>
            </div>
            <div class="col-md-2">
                <img src="{{asset('storage/logo/instagram.png')}}" alt="kontak" class="img-fluid img-custom" srcset="">
                <div class="mt-3">
                    <h5>E.MENARA.STORE</h5>
                </div>
            </div>
            <div class="col-md-2">
                <img src="{{asset('storage/logo/wa.png')}}" alt="kontak" class="img-fluid img-kontak" srcset="">
                <div class="mt-3">
                    <h5>087 794 893 498 </h5>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection