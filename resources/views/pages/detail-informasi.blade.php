@extends('layouts.home')
@section('content')
<div class="bg-white">
    <div class="my-3 mx-3">
        <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='currentColor'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
            <li class="breadcrumb-item" aria-current="page">Informasi</li>
            <li class="breadcrumb-item active" aria-current="page">{{$post->name}}</li>
        </ol>
        </nav>
    </div>
</div>
<div class="container-fluid" style="color:black">
    <div class="row">
    <h6 class="card-subtitle mb-2 text-muted my-2">Tanggal: {{$post->created_at->format('d, M Y')}}</h6>
        <div class="col-md-10 col-xs-12 text-center">
        <img src="{{asset('storage/'.$post->image)}}" class="img-thumbnail img-informasi" alt="{{$post->title}}">
            <div class="thumbnail">
                <h2>{{$post->title}}</h2>
                <p>{!! $post->body !!}</p>
            </div>
        </div>
        <div class="col-md-2 col-xs-12">
            <div class="mt-5">
                <h4>Informasi Lainnya</h4>
                <hr>
            </div>
            @foreach($posts as $row)
            <div class="text-center" {{$post->id == $row->id ? 'hidden':''}}>
                <a href="{{route('informasi.detail',$row)}}">
                    <img src="{{asset('storage/'.$row->image)}}" alt="{{$row->title}}" class="img-fluid" width="200" height="200">
                    <p class="py-2">{{$row->title}}</p>
                </a>
                <hr>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection