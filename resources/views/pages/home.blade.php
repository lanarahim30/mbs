@extends('layouts.home')
@section('content')
<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
  <ol class="carousel-indicators">
    @foreach($images as $key=>$image)
        <li data-bs-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
    @endforeach
  </ol>
  <div class="carousel-inner">
  @foreach($images as $key=>$image)
    <div class="carousel-item {{$key == 0 ? 'active' : '' }}">
      <img src="{{asset('storage/'.$image->image)}}" class="d-block w-100" alt="{{$image->name}}">
    </div>
    @endforeach
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </a>
</div>

<!-- Container (Portfolio Section) -->
<div class="bg-grey py-5" id="toko">
    <div class="container">
        <div class="text-center online-shop">
        <h2>Produk Kami</h2>
        <p style="font-size:25px">Mercusuar Buana Sejahtera mempunyai produk</p>
        <div class="row">
            @foreach($produks as $key=>$image)
                @if($key < 6 )
                <div class="col-sm-4 col-xs-12 text-center">
                <div class="bg-produk">
                    <div class="produk">
                      <div class="thumbnail">
                          <img src="{{asset('storage/'.$image->image)}}" alt="{{$image->name}}" class="img-fluid" width="200" height="200">
                          <p class="mt-3">{{$image->name}}</p>
                          <a href="{{route('produk',$image)}}" class="btn btn-produk">Selengkapnya</a>
                      </div>
                    </div>
                  </div>
                </div>
                @else 
                    break;
                @endif
            @endforeach
        </div>
        </div>
    </div>
</div>
<div id="portfolio" class="container-fluid text-center bg-white mt-5">
    <h2>Peta Lokasi</h2>
    <br>
    <div class="mapouter">
        <div class="gmap_canvas">
        <iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=kantor%20konsultan%20mbs&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
        </iframe><a href="https://sites.google.com/view/fmovies-to-org/fmovies"></a>
        </div>
        <!-- <style>.mapouter{position:relative;text-align:right;height:500px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:100%;}</style> -->
    </div>
</div>
@endsection