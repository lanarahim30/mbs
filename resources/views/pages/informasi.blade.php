@extends('layouts.home')
@section('content')
<div class="bg-white">
    <div class="my-3 mx-3">
        <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='currentColor'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Informasi</li>
        </ol>
        </nav>
    </div>
</div>
<div class="container" style="color:black">
    <div class="text-center">
        <h2>Informasi</h2>
    </div>
    <div class="informasi">
        @if($posts->count())
            @foreach($posts as $post)
                <div class="row">
                    <div class="col-md-1">
                        <div class="thumbnail">
                            <img src="{{asset('storage/'.$post->image)}}" alt="{{$post->title}}" srcset="" class="img-thumbnail">
                        </div>
                    </div>
                    <div class="col">
                        <p>{!! \Illuminate\Support\Str::words($post->body,50,'.....') !!}</p>
                        <p><a href="{{route('informasi.detail',$post)}}">Selengkapnya</a></p>
                    </div>
                </div>
                <hr>
            @endforeach
        @else
        <div class="alert alert-info">
                    
            Belum ada informasi

        </div>
        @endif
    </div>
</div>
@endsection