@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="text-center title">
                    <h1>Edit Galeri</h1>
                </div>
                <form action="{{route('edit.slider',$slider)}}" method="POST" enctype="multipart/form-data">
                    @method('patch')
                    @csrf
                    @include('pages.slider.form-control',['submit'=>'Edit'])
                </form>
            </div>
        </div>
    </div>
@endsection