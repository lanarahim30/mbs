@extends('layouts.admin')
@section('content')
    <div class="container">
    @if($sliders->count())
        <div class="text-center title mb-5">
            <h1>List Foto</h1>
        </div>
        <div class="row">
            @foreach ($sliders as $slider)
                <div class="col-md-4 mb-2">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="{{asset('storage/'.$slider->image)}}" alt="{{$slider->image}}">
                        <div class="card-body">
                            <p class="card-text">{{$slider->name}}</p>
                        </div>
                        <div class="card-footer d-flex justify-content-between">
                            <a href="{{ route('edit.slider' ,$slider) }}" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                            <!-- <a href="{{ route('delete.slider',$slider) }}" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a> -->
                            <button class="btn btn-sm btn-danger deleteSlider" data-sliderid="{{$slider->id}}"><i class="fas fa-trash"></i></button>
                        </div>
                    </div>
                </div>
            @endforeach
            @else 
                <div class="alert alert-info">
                    
                    Foto kosong

                </div>
            @endif
        </div>
        <div class="d-flex justify-content-center">
            <div>
                {{ $sliders->links() }}
            </div>
        </div>
    </div>
    <!-- modal  -->
    <div class="modal" tabindex="-1" id="deleteSlider">
        <div class="modal-dialog">
            <div class="modal-content">
            <form action="{{route('delete.slider')}}" method="POST">
                @method('delete')
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Delete Galeri</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                        <input type="hidden" name="id" id="slider_id">
                        <p>Anda ingin menghapus galeri ini !!!.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection