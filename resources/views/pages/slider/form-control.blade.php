<div class="form-group">
    <label for="image">Banner Slider</label>
    <input type="file" class="form-control-file" id="image" name="image">
    @error('image')
    <div class="text-danger mt-2">
        {{$message}}
    </div>
    @enderror
    @if(isset($slider->image))
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="{{asset('storage/'.$slider->image)}}" alt="Card image cap">
        </div>
    @endif
</div>
<div class="form-group">
    <label for="name">Nama Slider</label>
    <input type="text" class="form-control" name="name" placeholder="Masukan nama slider" value="{{ old('name') ?? $slider->name}}">
    @error('name')
    <div class="text-danger mt-2">
        {{$message}}
    </div>
    @enderror
</div>
<hr>
<div class="text-center mt-4">
    <a href="{{URL::previous()}}" class="btn btn-warning">Kembali</a>
    <button type="submit" class="btn btn-primary">{{isset($submit) ? $submit : 'Save'}}</button>
</div>