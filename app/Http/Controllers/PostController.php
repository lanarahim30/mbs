<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('pages.artikel.index',[
            'artikels' => Post::latest()->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('pages.artikel.create',[
            'artikel' => new Post()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $attr = $this->validateRequest();
        $attr['slug'] = \Str::slug($request->title);
        $attr['image'] = request('image') ? request()->file('image')->store('images/artikel') : 'images/no-image.png';
        $product = Post::create($attr);
        session()->flash('success','Artikel berhasil ditambahkan');
        return redirect()->route('create.artikel');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
        return view('pages.artikel.edit',[
            'artikel' => $post
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
        $attr = $this->validateRequest();
        if(request('image')){
            Storage::delete($post->image);
            $image = request()->file('image')->store('images/artikel');
        } elseif ($post->image){
            $image = $post->image;
        } else {
            $image = 'images/no-image.png';
        }
        $attr['image'] = $image;

        $post->update($attr);

        session()->flash('success','Update Berhasil');
        return redirect()->route('artikel');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
        $id = request('id');
        $artikel = Post::find($id);
        Storage::delete($artikel->image);
        $artikel->delete();
        session()->flash('success','Artikel Berhasil dihapus');
        return redirect()->route('artikel');
    }
    
    public function validateRequest()
    {
        return request()->validate([
            'title' => 'required',
            'body' => 'required'
        ]);
    }
}
