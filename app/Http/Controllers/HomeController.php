<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Slider,Profile,Galeri,Produk,Post};

class HomeController extends Controller
{
    public function dashboard()
    {
        return view('pages.dashboard');
    }
    public function home()
    {
        return view('pages.home',[
            'images' => Slider::orderBy('created_at', 'desc')->get(),
            'produks' => Produk::all()
        ]);
    }
    public function profile()
    {
        // dd(Profile::first());
        return view('pages.profilmbs',[
            'image' => Profile::first(),
            'produks' => Produk::all()
        ]);
    }
    public function galeri()
    {
        return view('pages.galeri',[
            'images' => Galeri::latest()->paginate(10),
            'produks' => Produk::all()
        ]);
    }
    public function kontak()
    {
        return view('pages.kontak',['produks' => Produk::all()]);
    }
    public function detail(Produk $produk)
    {
        return view('pages.detail-produk',[
            'produk' => $produk,
            'produks' => Produk::all()
        ]);
    }
    public function informasi()
    {
        return view('pages.informasi',[
            'posts' => Post::orderBy('created_at', 'desc')->get(),
            'produks' => Produk::all()
        ]);
    }
    public function informasiDetail(Post $post)
    {
        return view('pages.detail-informasi',[
            'post' => $post,
            'posts'=> Post::all(),
            'produks' => Produk::all()
        ]);
    }
}
