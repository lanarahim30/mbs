<?php

namespace App\Http\Controllers;

use App\Models\Galeri;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GaleriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('pages.galeri.index',[
            'galeris' => Galeri::latest()->paginate(20)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('pages.galeri.create',[
            'galeri' => New Galeri()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $attr = $this->validateRequest();
        $files = $request->file('images');
        if($request->hasfile('images'))
        {
            foreach($files as $image)
            {
                $data[] = $image ? $image->store('images/galeri') : 'null';
            }
        }
        foreach($data as $row)
        {
            Galeri::insert( [
                'image' => $row
            ]);
        }
        session()->flash('success','Galeri Berhasil dibuat');
        return redirect()->route('create.galeri');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Galeri  $galeri
     * @return \Illuminate\Http\Response
     */
    public function show(Galeri $galeri)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Galeri  $galeri
     * @return \Illuminate\Http\Response
     */
    public function edit(Galeri $galeri)
    {
        //
        return view('pages.galeri.edit',[
            'galeri'=>$galeri
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Galeri  $galeri
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Galeri $galeri)
    {
        //
        $attr = $this->validateRequest();
        if(request('image')){
            Storage::delete($galeri->image);
            $image = request()->file('image')->store('images/galeri');
        } elseif ($galeri->image){
            $image = $galeri->image;
        } else {
            $image = null;
        }
        $attr['image'] = $galeri;

        $slider->update($attr);

        session()->flash('success','Update Berhasil');
        return redirect()->route('create.galeri');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Galeri  $galeri
     * @return \Illuminate\Http\Response
     */
    public function destroy(Galeri $galeri)
    {
        //
        $id = request('id');
        $galeri = Galeri::find($id);
        Storage::delete($galeri->image);
        $galeri->delete();
        session()->flash('success','Galeri Berhasil dihapus');
        return redirect()->route('galeris');
    }
    public function validateRequest()
    {
        return request()->validate([
            'image' => request('image') ? 'required|image|mimes:jpeg,jpg,png,gif|max:10000':''
        ]);
    }
}
