<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('pages.slider.index',[
            'sliders' => Slider::latest()->paginate(5)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.slider.create',[
            'slider' => New Slider()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $attr = $this->validateRequest();
        $attr['slug'] = \Str::slug($request->name);
        $attr['image'] = request('image') ? request()->file('image')->store('images/banner') : 'null';

        $slider = Slider::create($attr);

        session()->flash('success','Slider Berhasil dibuat');
        return redirect()->route('create.slider');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\c  $c
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\c  $c
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        //
        return view('pages.slider.edit',[
            'slider'=>$slider
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\c  $c
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        //
        $attr = $this->validateRequest();
        if(request('image')){
            Storage::delete($slider->image);
            $image = request()->file('image')->store('images/banner');
        } elseif ($slider->image){
            $image = $slider->image;
        } else {
            $image = null;
        }
        $attr['image'] = $image;

        $slider->update($attr);

        session()->flash('success','Update Berhasil');
        return redirect()->route('create.slider');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\c  $c
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        
        $id = request('id');
        $slider = Slider::find($id);
        Storage::delete($slider->image);
        $slider->delete();
        session()->flash('success','Slider Berhasil dihapus');
        return redirect()->route('sliders');
    }
    public function validateRequest()
    {
        return request()->validate([
            'name' => 'required',
            'image' => request('image') ? 'image|mimes:jpeg,jpg,png,gif|max:10000':''
        ]);
    }
}
