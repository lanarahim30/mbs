<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('pages.product.index',[
            'products' => Produk::latest()->paginate(10)
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('pages.product.create',[
            'product' => New Produk()
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $attr = $this->validateRequest();
        $attr['slug'] = \Str::slug($request->name);
        $attr['image'] = request('image') ? request()->file('image')->store('images/product') : 'null';
        $attr['description'] = $request->description;
        $product = Produk::create($attr);
        session()->flash('success','Product berhasil ditambahkan');
        return redirect()->route('create.produk');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function show(Produk $produk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function edit(Produk $produk)
    {
        //
        return view('pages.product.edit',[
            'product' => $produk
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produk $produk)
    {
        //
        $attr = $this->validateRequest();
        if(request('image')){
            Storage::delete($produk->image);
            $image = request()->file('image')->store('images/product');
        } elseif ($produk->image){
            $image = $produk->image;
        } else {
            $image = null;
        }
        $attr['image'] = $image;
        $attr['description'] = $request->description;
        // die($attr);
        $produk->update($attr);
        session()->flash('success','Product berhasil diedit');
        return redirect()->route('create.produk');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produk $produk)
    {
        //
        $id = request('id');
        $product = Produk::find($id);
        Storage::delete($product->image);
        $product->delete();
        session()->flash('success','Product Berhasil dihapus');
        return redirect()->route('produks');

    }
    public function validateRequest()
    {
        return request()->validate([
            'name' => 'required',
            'image' => request('image') ? 'image|mimes:jpeg,jpg,png,gif|max:20000':''
        ]);
    }
    public function upload(Request $request)
    {
        
        if ($request->hasFile('upload')) {
            $file = $request->file('upload'); 
            $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            
            $fileName = $fileName . '_' . time() . '.' . $file->getClientOriginalExtension();

            $file->move(public_path('uploads'), $fileName);
            
            $ckeditor = $request->input('CKEditorFuncNum');
            $url = asset('uploads/' . $fileName); 
            $msg = 'Image uploaded successfully'; 

            $response = "<script>window.parent.CKEDITOR.tools.callFunction($ckeditor, '$url', '$msg')</script>";

            //SET HEADERNYA
            @header('Content-type: text/html; charset=utf-8'); 
            return $response;
        }
    }
}
